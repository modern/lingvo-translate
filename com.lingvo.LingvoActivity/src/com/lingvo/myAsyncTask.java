package com.lingvo;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

/**
 * ������ ������� � ����
 * @author VADIM
 *
 */
public class myAsyncTask extends AsyncTask<Void, Integer, Void> {
	/**
	 * �������� ������ � ������ Variables
	 */
	private Variables v; 
	
    @SuppressWarnings("static-access")
	@Override
    protected void onPreExecute() {
        super.onPreExecute();

        v.imageProgress.setVisibility(View.VISIBLE);
        v.ProgressBar = (AnimationDrawable)Variables.imageProgress.getDrawable();
        v.mainwebview.clearView(); 
        v.webViewTran.clearView(); 
    }
    
    @SuppressWarnings("static-access")
	@Override
    protected Void doInBackground(Void... params) {

    	// ������� �������, ��� ����� ����� ������: only one looper may be created per thread
    	
    	if(Looper.myLooper() == null) {
    		Looper.prepare(); // ��� ����� ���������� ������, ��� ��� ���������� �� � �������� ����������� ������
    	}
    	   	
    	Parser parser = new Parser();
    	v.src_song1 = ""; //������� ������ ����������
    	v.src_song2 = "";

    	try {
    		//Looper.prepare(); 
    		v.html = parser.Parserus(v.go_url);
    		v.tmpImg = v.html[0];
    		v.tmpHtml  = v.html[1];
			
    		v.src_song1  = v.html[2];
    		v.src_song2  = v.html[3];
			
			if (v.src_song1 == "") v.bs1 = 1; // ������-�� ��-�� equals ���� �������
			if (v.src_song2 == "") v.bs2 = 1;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
        return null;
    }
    
    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }
    
    @SuppressWarnings("static-access")
	@Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"; // ��������� �������� � ����������
        String s = "<html><body>";
        String e = "</body></html>";
        

        v.webViewTran.loadData(s + v.tmpImg + e, "text/html", "UTF-8");
        v.mainwebview.loadDataWithBaseURL("file:///android_asset/.", header + v.tmpHtml, "text/html", "UTF-8", null);

        
        
        if (v.bs1 !=1) {
        	v.buttonPlayPauseSong1.setVisibility(View.VISIBLE);
        } 
        
        if (v.bs2 !=1) {
        	v.buttonPlayPauseSong2.setVisibility(View.VISIBLE);
        }

		// ��� �����������, ���������, ������� �� ������� � ����������� � �� ����� �� ������ �� �����
		if (v.url_from.equals("en") && v.bs1 !=1 && v.bs2 !=1) {
			v.tame.setVisibility(View.VISIBLE);
			v.teng.setVisibility(View.VISIBLE);
		} else {
			//webViewSong1.setPadding(10, 0, 0, 0); // ������ ������, ��� ���������
			v.tame.setVisibility(View.GONE);
			v.teng.setVisibility(View.GONE);
		}
		Variables.imageProgress.setVisibility(View.GONE);
		//v.hide.setVisibility(View.GONE);

    } 
}
