package com.lingvo;

import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;

public class EventsHandlers implements OnClickListener {
	/**
	 * �������� ������ � ������ Variables
	 */
	private static Variables v;
	
	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.ViewSong1) { // ���������� ������� ��� ������ ����1
			try { 
				v.mediaPlayer.setDataSource(v.audiourl.toString()); 
				v.mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer. 
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(!v.mediaPlayer.isPlaying()) {
				v.mediaPlayer.start();
				v.buttonPlayPauseSong1.setImageResource(R.drawable.song_stop);
			} else {
				v.mediaPlayer.pause();
				v.buttonPlayPauseSong1.setImageResource(R.drawable.song_play);
			}
			
		} else if (view.getId() == R.id.ViewSong2) { // ���������� ������� ��� ������ ����2
			try {
				v.mediaPlayer.setDataSource(v.audiourl.toString()); 
				v.mediaPlayer.prepare(); 
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(!v.mediaPlayer.isPlaying()) {
				v.mediaPlayer.start();
				v.buttonPlayPauseSong2.setImageResource(R.drawable.song_stop);
			} else {
				v.mediaPlayer.pause();
				v.buttonPlayPauseSong2.setImageResource(R.drawable.song_play);
			}
		} else if (view.getId() == R.id.button_translations) { // ������ ��������
			v.button_translations.setBackgroundResource(R.drawable.bottom_button_press);
			v.button_examples.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_revs.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_interpretation.setBackgroundResource(R.drawable.bottom_button_background);
			
			v.button_translations.setTextColor(Color.parseColor("#FFFFFF"));
			v.button_examples.setTextColor(Color.parseColor("#000000"));
			v.button_revs.setTextColor(Color.parseColor("#000000"));
			v.button_interpretation.setTextColor(Color.parseColor("#000000"));
			
			v.whatButPres = "button_translations";
			
		} else if (view.getId() == R.id.button_examples) { // ������ ��������
			v.button_examples.setBackgroundResource(R.drawable.bottom_button_press);
			v.button_translations.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_revs.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_interpretation.setBackgroundResource(R.drawable.bottom_button_background);
			
			v.button_examples.setTextColor(Color.parseColor("#FFFFFF"));
			v.button_translations.setTextColor(Color.parseColor("#000000"));
			v.button_revs.setTextColor(Color.parseColor("#000000"));
			v.button_interpretation.setTextColor(Color.parseColor("#000000"));
			
			v.whatButPres = "button_examples";
			
		} else if (view.getId() == R.id.button_revs) { // ������ ��������
			v.button_revs.setBackgroundResource(R.drawable.bottom_button_press);
			v.button_translations.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_examples.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_interpretation.setBackgroundResource(R.drawable.bottom_button_background);
			
			v.button_revs.setTextColor(Color.parseColor("#FFFFFF"));
			v.button_translations.setTextColor(Color.parseColor("#000000"));
			v.button_examples.setTextColor(Color.parseColor("#000000"));
			v.button_interpretation.setTextColor(Color.parseColor("#000000"));
			
			v.whatButPres = "button_revs";
			
		} else if (view.getId() == R.id.button_interpretation) { // ������ ����������
			v.button_interpretation.setBackgroundResource(R.drawable.bottom_button_press);
			v.button_translations.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_examples.setBackgroundResource(R.drawable.bottom_button_background);
			v.button_revs.setBackgroundResource(R.drawable.bottom_button_background);
			
			v.button_interpretation.setTextColor(Color.parseColor("#FFFFFF"));
			v.button_translations.setTextColor(Color.parseColor("#000000"));
			v.button_examples.setTextColor(Color.parseColor("#000000"));
			v.button_revs.setTextColor(Color.parseColor("#000000"));
			
			v.whatButPres = "button_interpretation";
			
		} else if (view.getId() == R.id.imageButtonSwitch) { // ���������� ������� ��� button_translations (����������� �����)
			
			// ������ ������� �������� ���� � ���� �������� 
			String posSL = String.valueOf(v.sl.getSelectedItem()); //�������� ������� �������� sl
			String posSR = String.valueOf(v.sr.getSelectedItem()); //�������� ������� �������� sr

			ArrayAdapter myAdapSL = (ArrayAdapter) v.sl.getAdapter(); //cast to an ArrayAdapter
			ArrayAdapter myAdapSR = (ArrayAdapter) v.sr.getAdapter(); //cast to an ArrayAdapter
			
			int PositionSL = myAdapSL.getPosition(posSL);
			int PositionSR = myAdapSR.getPosition(posSR);

			//������������� �������
			v.sl.setSelection(PositionSR);
			v.sr.setSelection(PositionSL);
			
		} else if (view.getId() == R.id.cleartextfield) { // ���������� ������� ��� imagebuttonclear
			v.search_textedit.setText("");			
		} 
	}	
}


