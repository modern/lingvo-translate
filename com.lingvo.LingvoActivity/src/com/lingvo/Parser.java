package com.lingvo;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.os.AsyncTask;

/**
 * ��� ��� ��������: ������� ����� �����, �������� ��� ������, �������� ����� � ���� ������
 * @author VADIM
 *
 */
public class Parser {
	
	LingvoActivity la = new LingvoActivity();
	
	public String css;
	
	String res;
	
	String[] Parserus(String url) throws IOException {
		
		// ������������� ����������
		Document doc = Jsoup.connect(url).referrer("http://www.google.com").userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.83 Safari/537.1").get();
		
		Elements searchresults = doc.getElementsByClass("b-search-results");    // �������� �� ���������� ������ ��� ��� 

		String text = searchresults.toString(); 
		Document docu = Jsoup.parse(text);
		Elements elements = docu.select("div"); 
		elements.get(1).remove(); // ������

		
		// ������� �������� ��������
		docu.select("div[class=b-glossary border-sea]").remove();
		docu.select("div[class=bookmark-abbyy]").remove();
		docu.select("div[class=b-comments-facebook]").remove();
		docu.select("div[class=b-right-panel]").remove();
		docu.select("div[class=b-user-links]").remove();
		docu.select("div[class=ft]").remove(); 
		docu.select("div[class=b-round-box_cont]").remove();
		docu.select("div[class=b-round-box_reverse-warn b-round-box]").remove();
		docu.select("div[class=source]").remove();
		docu.select("div[class=b-glossary_users data-empty border-sky]").remove();
		docu.select("span[class=g-article__abbrev]").remove();
		
		// �������� ��� ���� � �������������, �������� � ������� ������
		Elements srcimages = docu.select("img"); 

		StringBuffer stb = new StringBuffer(srcimages.toString());
		int ind = stb.indexOf("src=\""); 
		int tmp; 
		
		int indl = stb.indexOf("<img"); // ������ ��������� ����, ��� ������� [
		int tmpl; // tmp link 
		

		
		while(ind != -1 && indl != -1 ) { 
			
			tmp = ind + 5; // ����� src="
			tmpl = indl; 
			
			
			stb.insert(tmp, "http://www.lingvo.ua"); // ��������� ����� src="
			stb.insert(tmpl, "[");
			
			
			ind = stb.indexOf("src=\"", tmp); // ������ ������ ����� tmp
			indl = stb.indexOf("<img", tmpl+5); 
			
		} 
		
		
		int indle = stb.indexOf(">"); // ������ ��������� ����, ��� ������� [
		int tmple; // tmp link end
		
		while (indle != -1) {
			tmple = indle + 1;
			stb.insert(tmple, "]");
			indle = stb.indexOf(">", tmple);
		}

		// �������� ������ �� ����
		Elements srcsongs = docu.select("span[class=soundNoScript]"); // �������� ��� � ��������
		StringBuffer srcson = new StringBuffer(srcsongs.toString());
		int indStart = srcson.indexOf("FileName="); // ������ ������� ���������
		int indEnd  = srcson.indexOf("=%26Di"); // ������ ����� ���������
		ArrayList<String> list = new ArrayList<String>();
		
		while (indStart != -1) { // ������� ����� �����

			list.add(srcson.substring(indStart + 9, indEnd)); 	
			indStart = srcson.indexOf("FileName=", indEnd);
			indEnd  = srcson.indexOf("=%26Di", indStart);

		}		
		// ������ �� ��������
		for (int lo = 0; lo < list.size(); lo++) {
			for (int lol = 0; lol < list.size(); lol++) {				
				if (list.get(lo).equals((list.get(lol)))) {
					list.remove(lo);
				} 
			}
		}		
		// �� ������ �������� ��� ����� �� �������, ������� ������ ����� � ������� ����������
		// ��� ��� �� �������� ���� �������
		
		
		docu.select("noscript").remove();
		docu.select("span[class=soundAll js-voice-trans]").remove();
		docu.select("span[class=name-dictionaries]").remove(); 
		docu.select("img[class=g-article__transcription]").remove(); 
		docu.select("span[class=tab]").remove(); 
		docu.select("div[class=b-button b-button_blue b-button_add b-button_add-glossary]").remove();
		

		elements = docu.select("p");
		elements.get(0).remove();
		
		elements = docu.select("div"); 
		elements.get(1).remove();
		
		
		docu.head().appendElement("link").attr("rel", "stylesheet").attr("type", "text/css").attr("href", "css_standard");
		

		String result = docu.outerHtml();
		
		String s_stb = stb + "" ;
		String[] im = {s_stb, result, list.get(1), list.get(0)};
		
		return im;
		
	}
	
	/*
	// �������� ����� css �� ����� ��������
	private String readCss() {
    	InputStream inputStream = la.context.getResources().openRawResource(R.raw.css_standard);
    	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    	
    	int i;
    	
    	try {
    		i = inputStream.read();
    		   while (i != -1) {
    			   byteArrayOutputStream.write(i);
    		       i = inputStream.read();
    		       }
    		   inputStream.close();
    		   } catch (IOException e) {	    			   
    			   }
    	return byteArrayOutputStream.toString();
	}
	*/

}
