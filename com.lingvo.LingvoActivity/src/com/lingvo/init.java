package com.lingvo;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class init extends LingvoActivity {
	
	/**
	 *  http://stackoverflow.com/questions/8323777/using-findviewbyid-in-a-class-that-does-not-extend-activity-in-android
	 */
	public Activity activity; 
	/**
	 * �������� ������ � ������ Variables
	 */
	private Variables v; 
	
	@SuppressWarnings("static-access") // ����� �� ���� ����������� ��������������
	init (Activity activity) {
		this.activity = activity;
		
		// ������� ���������
    	v.home_scroll = (ScrollView)this.activity.findViewById(R.id.scrollView1);
		
		v.imageProgress = (ImageView)this.activity.findViewById(R.id.imageProgress); // �������� ���������
		v.image_btn_voice = (ImageButton)this.activity.findViewById(R.id.image_btn_voice); // �������� ������������� ������
		
		v.tame = (TextView)this.activity.findViewById(R.id.textTrAmer); // ��������� ������������� ����. ����.
    	v.teng = (TextView)this.activity.findViewById(R.id.textTrEngl);
    	v.tame.setVisibility(View.GONE);
		v.teng.setVisibility(View.GONE);
		
		//v.hide = (ImageView)this.activity.findViewById(R.id.imageView1); // ��������� ��������
		
    	v.webViewTran  = (WebView)this.activity.findViewById(R.id.webViewTran); // ������ � �������������

    	// ���������� ��� ��������������� ����� 
    	v.mediaPlayer = new MediaPlayer();
    	v.mediaPlayer.setOnBufferingUpdateListener(this);
    	v.mediaPlayer.setOnCompletionListener(this);
		
		// test
    	v.audiourl = Uri.parse("http://www.lingvo.ua/Sound?FileName=NzYyNS53YXY=&DictionaryName=LingvoUniversal%20(En-Ru)");

    	// ������ ��������������� ������, ��������� ��� ��� � ���������
    	v.buttonPlayPauseSong1 = (ImageButton)this.activity.findViewById(R.id.ViewSong1);
    	v.buttonPlayPauseSong2 = (ImageButton)this.activity.findViewById(R.id.ViewSong2);
    	v.buttonPlayPauseSong1.setOnClickListener(new EventsHandlers());
    	v.buttonPlayPauseSong2.setOnClickListener(new EventsHandlers());
    	v.buttonPlayPauseSong1.setVisibility(View.GONE);
    	v.buttonPlayPauseSong2.setVisibility(View.GONE);
    	
    	// �������� ���������� �� 180%
    	v.webViewTran.setInitialScale(180);
    	
    	v.RelativeLayoutWebView = (RelativeLayout)this.activity.findViewById(R.id.RelativeLayoutWebView);
        
        // ������� ������ ������� ���� � ��������, ������ ����������
    	v.button_translations = (Button)this.activity.findViewById(R.id.button_translations);
    	v.button_translations.setOnClickListener(new EventsHandlers());
        
    	v.button_examples = (Button)this.activity.findViewById(R.id.button_examples);
    	v.button_examples.setOnClickListener(new EventsHandlers());
        
    	v.button_revs = (Button)this.activity.findViewById(R.id.button_revs);
    	v.button_revs.setOnClickListener(new EventsHandlers());
        
    	v.button_interpretation = (Button)this.activity.findViewById(R.id.button_interpretation);
    	v.button_interpretation.setOnClickListener(new EventsHandlers());
    	
        // ������������� ������
    	v.imageButtonSwitch = (ImageButton)this.activity.findViewById(R.id.imageButtonSwitch);
    	v.imageButtonSwitch.setOnClickListener(new EventsHandlers());
        
        // ����� ������
    	v.sl = (Spinner)this.activity.findViewById(R.id.spinnerChoiceLanguageLeft);
    	v.sl.setOnItemSelectedListener(new SpinnerOnItemSelectedListener());
    	// ������ ������
    	v.sr = (Spinner)this.activity.findViewById(R.id.spinnerChoiceLanguageRight);
    	
    	// ������ ���������� ���������� ����
    	v.cleartextfield = (ImageButton)this.activity.findViewById(R.id.cleartextfield);
    	v.cleartextfield.setOnClickListener(new EventsHandlers());
    	
    	// ��������� ���� � ������� ��� �������
    	v.search_textedit = (AutoCompleteTextView)this.activity.findViewById(R.id.search_textedit); 
	   	 
    	// ������ � ���������
    	v.mainwebview  = (WebView)this.activity.findViewById(R.id.MainWebView); 

    	// ������ ������ ������
    	v.image_btn_go = (ImageButton)this.activity.findViewById(R.id.image_btn_go); 
    	v.image_btn_go.setOnClickListener(new ClickImageButtonGo());
    	
    	// ������ ����������
    	v.ya_search = (TextView)this.activity.findViewById(R.id.ya_search);
    	v.imageline2 = (ImageView)this.activity.findViewById(R.id.ImageLine2);
    	v.imageline3 = (ImageView)this.activity.findViewById(R.id.ImageLine3);

	}
}
