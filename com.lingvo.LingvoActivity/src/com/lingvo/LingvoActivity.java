package com.lingvo;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.slidingmenu.MyHorizontalScrollView;
import com.slidingmenu.MyHorizontalScrollView.SizeCallback;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
//import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

public class LingvoActivity extends Activity implements OnBufferingUpdateListener, OnCompletionListener, TextWatcher {

	public static Context mcontext;
	
	MyHorizontalScrollView scrollView;
	View menu;
    View app;
    ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;

	/**
	 * �������� ������ � ������ Variables
	 */
	private static Variables v; 
	/**
	 * ��� ������� � ��
	 */
	private static ItemsDbAdapter dbHelper;
	

	static long second, ticker, uptimemilli; // ��� ������ �������������� ����� ������� ���������

	static private short tmpSong = 0; // ��� ������������ �������� �������� ������

    @SuppressWarnings("static-access") // ����� �� ���� ����������� ��������������
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE); // �������� �����
        
        mcontext = getApplicationContext();
        
        // start ScrollView
        LayoutInflater inflater = LayoutInflater.from(this);
        scrollView = (MyHorizontalScrollView) inflater.inflate(R.layout.horz_scroll_with_list_menu, null);
        setContentView(scrollView);
        
        menu = inflater.inflate(R.layout.horz_scroll_menu, null);
        app = inflater.inflate(R.layout.lingvo_activity, null);
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBar);
        
        btnSlide = (ImageView) tabBar.findViewById(R.id.BtnSlide);
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu));
        
        final View[] children = new View[] { menu, app };
        
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
        // end ScrollView

    	// �������� ��������
        v.MainContext = getApplicationContext();
        
        // ��������� ����������� � ��
        dbHelper = new ItemsDbAdapter(this);
        dbHelper.open();
        
        ItemAutoTextAdapter adapter = new ItemAutoTextAdapter(dbHelper);
        
        new init(this); // ������������� �����������
        
        v.search_textedit.addTextChangedListener(new YaDoInBackground()); // ��������� ��������� ���������������
        
        // ���������� �������� ��������
        ArrayAdapter<CharSequence> adapterSL = ArrayAdapter.createFromResource(this,
                    R.array.default_language,
                    R.layout.spinner_text_style);
        adapterSL.setDropDownViewResource(R.layout.spinner_list_style);
        v.sl.setAdapter(adapterSL);
        
        ArrayAdapter<CharSequence> adapterSR = ArrayAdapter.createFromResource(this,
                    R.array.ru_language,
                    R.layout.spinner_text_style);
        adapterSR.setDropDownViewResource(R.layout.spinner_list_style);
        v.sr.setAdapter(adapterSR);
        
        /* ����� ������� */
        
        v.search_textedit.setAdapter(adapter);
        v.search_textedit.setOnItemClickListener(adapter);

		// ���������, �������� �� ������������� ������ � ��������
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() != 0) {
        	v.image_btn_voice.setOnClickListener(new ClickAndSpeak());   //setOnClickListener(this);
        } else {
        	v.image_btn_voice.setEnabled(false);
        	Toast.makeText(this , "��������� ���� ����������." , Toast.LENGTH_SHORT).show();
        }
        
        YaProper ya_proper = new YaProper(this);
        
        
    }   
    
    /**
     * ��� ������ ���� "� ��������� Lingvo Online"
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	super.onCreateOptionsMenu(menu);
    	
    	MenuItem infoBtn = menu.add(R.string.aboutOption);
    	infoBtn.setIcon(R.drawable.about);

    	return true;
    	
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper  != null) {
            dbHelper.close();
        }
    }
    
    public void onTextChanged(CharSequence s, int start, int before,
    		int count){
    }
    
    public void beforeTextChanged(CharSequence s, int start,
    		int count, int after){
    }
    
    public void afterTextChanged(Editable s){	
    }
    
    /**
     * Helper for examples with a HSV that should be scrolled by a menu View's width.
     */
    static class ClickListenerForScrolling implements OnClickListener {
        HorizontalScrollView scrollView;
        View menu;
        /**
         * Menu must NOT be out/shown to start with.
         */
        boolean menuOut = false;

        public ClickListenerForScrolling(HorizontalScrollView scrollView, View menu) {
            super();
            this.scrollView = scrollView;
            this.menu = menu;
        }

        @Override
        public void onClick(View v) {

            int menuWidth = menu.getMeasuredWidth();

            // Ensure menu is visible
            menu.setVisibility(View.VISIBLE);

            if (!menuOut) {
                // Scroll to 0 to reveal menu
                int left = 0;
                scrollView.smoothScrollTo(left, 0);
            } else {
                // Scroll to menuWidth so menu isn't on screen.
                int left = menuWidth;
                scrollView.smoothScrollTo(left, 0);
            }
            menuOut = !menuOut;
        }
    }
    
    /**
     * Helper that remembers the width of the 'slide' button, so that the 'slide' button remains in view, even when the menu is
     * showing.
     */
    static class SizeCallbackForMenu implements SizeCallback {
        int btnWidth;
        View btnSlide;

        public SizeCallbackForMenu(View btnSlide) {
            super();
            this.btnSlide = btnSlide;
        }

        @Override
        public void onGlobalLayout() {
            btnWidth = btnSlide.getMeasuredWidth();
            System.out.println("btnWidth=" + btnWidth);
        }

        @Override
        public void getViewSize(int idx, int w, int h, int[] dims) {
            dims[0] = w;
            dims[1] = h;
            final int menuIdx = 0;
            if (idx == menuIdx) {
                dims[0] = w - btnWidth;
            }
        }
    }

    
 
    // ������� �� ��������� �������� ����������, ���������� ���������
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
    	super.onSaveInstanceState(savedInstanceState);
    	//v.mainwebview.saveState(savedInstanceState);
    	//v.webViewTran.saveState(savedInstanceState);
    }
    
    // ������� �� ��������� �������� ����������, �������������� ���������
    @SuppressWarnings("static-access")
	@Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        
        //v.mainwebview.restoreState(savedInstanceState);
        //v.webViewTran.restoreState(savedInstanceState);
 
        // �� ������ ���������� ����� ������ ���� ������ ���������, ������ �� ��������� ��� � ���������
        if(v.whatButPres.equals("button_translations")) {
        	EventsHandlers ct = new EventsHandlers();
        	ct.onClick(v.button_translations);
        } else if (v.whatButPres.equals("button_examples")) {
        	EventsHandlers ce = new EventsHandlers();
        	ce.onClick(v.button_examples);
        } else if (v.whatButPres.equals("button_revs")) {
        	EventsHandlers cr = new EventsHandlers();
        	cr.onClick(v.button_revs);
        } else if (v.whatButPres.equals("button_interpretation")) {
        	EventsHandlers ci = new EventsHandlers();
        	ci.onClick(v.button_interpretation);
        }
    }

    // ���������� ������� ��� ������ ����������� ������ 
    static class SpinnerOnItemSelectedListener implements OnItemSelectedListener {

		@SuppressWarnings("static-access")
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
			// ������������� hint text
			String hint_text = String.valueOf(v.sl.getSelectedItem()); 
			v.search_textedit.setHint(hint_text);
			//������������� ���� ���� ���������� �����
			v.searchlang = String.valueOf(v.sl.getSelectedItem());
			
	    	if (v.searchlang.equals("�������")) {
	    		v.searchlang = "RU";
	    	} else if (v.searchlang.equals("����������")) {
	    		v.searchlang = "en_US";
	    	} else if (v.searchlang.equals("���������")) {
	    		v.searchlang = "ES";
	    	} else if (v.searchlang.equals("�����������")) {
	    		v.searchlang = "IT";
	    	} else if (v.searchlang.equals("��������")) {
	    		v.searchlang = "DE";
	    	} else if (v.searchlang.equals("����������")) {
	    		Toast.makeText(v.MainContext , "���������� ��������� ���� ����������. ������ �������." , Toast.LENGTH_SHORT).show();
	    		v.searchlang = "RU";
	    	} else if (v.searchlang.equals("�����������")) {
	    		v.searchlang = "FR";
	    	}

	    	spinners.SetSpinnerItems(hint_text);
	    	
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
		}
    }

    // ��������� ������ ���������� ����� ������
    private class ClickAndSpeak implements OnClickListener {
		@Override
		public void onClick(View arg0) {
			startVoiceRecognitionActivity();
		}
    }
    
    
    /**
     * ������ ������������� ����
     */
    
    @SuppressWarnings("static-access")
	private void startVoiceRecognitionActivity() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, v.searchlang); // ������������� ������� ���� 
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1); // ������������ ���������� ����������� ����
        //intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speech recognition demo");
        startActivityForResult(intent, v.VOICE_RECOGNITION_REQUEST_CODE);
        
    } 
    
    /**
     * ��������� ���������� ������
     */
    @SuppressWarnings("static-access")
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == v.VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            // Fill the list view with the strings the recognizer thought it could have heard
        	ArrayList<String> matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
        	String resString = "";
        	for (String s : matches)
        	{
        		resString += s + "\t";
        	}
        	v.search_textedit.setText(resString.trim());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
  
    // ���������� ������� ��� imagebuttongo, ������ ������
    static class ClickImageButtonGo implements OnClickListener {
    	
    	String url = "http://www.lingvo.ua/ru/";
    	String lang_to;
    	String bottom_bar = "Translate/";
    	String url_to;
    	String get_text;
    	String html;
    	
		@SuppressWarnings("static-access")
		public void onClick(View arg) {
			
			String text = v.search_textedit.getText().toString();
			dbHelper.createItem(text);
			//Toast.makeText(v.MainContext, "�������� ������ � ��", Toast.LENGTH_SHORT).show();
			
			// �������� �������� ��������� � ���������� ����
			v.lang_from = String.valueOf(v.sl.getSelectedItem());
	    	lang_to = String.valueOf(v.sr.getSelectedItem());
	    	get_text = v.search_textedit.getText().toString(); 
	    	try {
				get_text = URLEncoder.encode(get_text, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			// ���������� ��������� ���� (from)
	    	if (v.lang_from.equals("�������")) {
	    		v.url_from = "ru";
	    	} else if (v.lang_from.equals("����������")) {
	    		v.url_from = "en";
	    	} else if (v.lang_from.equals("���������")) {
	    		v.url_from = "es";
	    	} else if (v.lang_from.equals("�����������")) {
	    		v.url_from = "it";
	    	} else if (v.lang_from.equals("��������")) {
	    		v.url_from = "de";
	    	} else if (v.lang_from.equals("����������")) {
	    		v.url_from = "uk";
	    	} else if (v.lang_from.equals("�����������")) {
	    		v.url_from = "fr";
	    	}
	    	
			// ���������� ��������� ���� (to)
	    	if (lang_to.equals("�������")) {
	    		url_to = "ru/";
	    	} else if (lang_to.equals("����������")) {
	    		url_to = "en/";
	    	} else if (lang_to.equals("���������")) {
	    		url_to = "es/";
	    	} else if (lang_to.equals("�����������")) {
	    		url_to = "it/";
	    	} else if (lang_to.equals("��������")) {
	    		url_to = "de/";
	    	} else if (lang_to.equals("����������")) {
	    		url_to = "uk/";
	    	} else if (lang_to.equals("�����������")) {
	    		url_to = "fr/";
	    	}
	    	
	    	// ������ ������ �������
	    	v.go_url = url + bottom_bar + v.url_from + "-" + url_to + get_text;
	    	
	    	myAsyncTask mAT = new myAsyncTask();
	    	mAT.execute();
	    	
	    	
			} 
		
		}

	@SuppressWarnings("static-access")
	@Override
	public void onCompletion(MediaPlayer mp) {
		v.buttonPlayPauseSong1.setImageResource(R.drawable.song_play);
		v.buttonPlayPauseSong2.setImageResource(R.drawable.song_play);
		
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		
	}
}
