package com.lingvo;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.os.AsyncTask;
import android.os.Looper;
import android.text.Editable;
import android.widget.TextView;

public class YaDoInBackground extends AsyncTask<String, Void, Void> implements android.text.TextWatcher {
	
	private Variables var;
	private String searchresults;
	
	public void afterTextChanged(Editable arg0) {
	}

	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}
	
	public void onTextChanged(CharSequence s, int start, int before,
			int count) {
		YaDoInBackground YDIB = new YaDoInBackground();
		YDIB.execute();
	}
	
	@Override
	protected Void doInBackground(String... arg0) {
		
    	if(Looper.myLooper() == null) {
    		Looper.prepare();
    	}
    	
    	StringBuffer tContent = new StringBuffer(var.search_textedit.getText().toString());
    	
    	int ind = tContent.indexOf(" "); 
    	int tmp; 
    	while(ind != -1) { 
    		tmp = ind + 1; 
    		tContent.replace(ind, ind+1, "+");
    		ind = tContent.indexOf(" ", tmp);
    	}
    	
    	try {
			Document doc = Jsoup.connect("http://translate.yandex.net/api/v1/tr/translate?lang=en-ru&text=" + tContent).referrer("http://www.google.com").userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.83 Safari/537.1").get();
			searchresults = doc.text();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        var.ya_search.setText(searchresults);
	}
}
