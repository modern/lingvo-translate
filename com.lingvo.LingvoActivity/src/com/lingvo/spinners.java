package com.lingvo;

import java.util.ArrayList;

import android.R.string;
import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class spinners extends LingvoActivity {
	
	private static Variables v;
	LingvoActivity la;
	
	/**
	 * ������ �������� ������ �� ��������.
	 * �� ���� ������ ������ �� ��������� �������� ������.
	 * ������� ����� �������� ���: String.valueOf(v.sl.getSelectedItem())
	 * @param String current_item
	 */

	static void SetSpinnerItems(String current_item) {

		if (current_item.equals("�������")) {
			ArrayAdapter<CharSequence> adapterRu = ArrayAdapter.createFromResource(mcontext,
                    R.array.ru_language,
                    R.layout.spinner_text_style);
			adapterRu.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterRu);
		} else if (current_item.equals("����������")) {
			ArrayAdapter<CharSequence> adapterEn = ArrayAdapter.createFromResource(mcontext,
                    R.array.en_language,
                    R.layout.spinner_text_style);
			adapterEn.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterEn);
		} else if (current_item.equals("���������")) {
			ArrayAdapter<CharSequence> adapterEs = ArrayAdapter.createFromResource(mcontext,
                    R.array.es_language,
                    R.layout.spinner_text_style);
			adapterEs.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterEs);
		} else if (current_item.equals("�����������")) {
			ArrayAdapter<CharSequence> adapterIt = ArrayAdapter.createFromResource(mcontext,
                    R.array.it_language,
                    R.layout.spinner_text_style);
			adapterIt.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterIt);
		} else if (current_item.equals("��������")) {
			ArrayAdapter<CharSequence> adapterDe = ArrayAdapter.createFromResource(mcontext,
                    R.array.de_language,
                    R.layout.spinner_text_style);
			adapterDe.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterDe);
		} else if (current_item.equals("����������")) {
			ArrayAdapter<CharSequence> adapterUk = ArrayAdapter.createFromResource(mcontext,
                    R.array.uk_language,
                    R.layout.spinner_text_style);
			adapterUk.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterUk);
		} else if (current_item.equals("�����������")) {
			ArrayAdapter<CharSequence> adapterFr = ArrayAdapter.createFromResource(mcontext,
                    R.array.fr_language,
                    R.layout.spinner_text_style);
			adapterFr.setDropDownViewResource(R.layout.spinner_list_style);
			v.sr.setAdapter(adapterFr);
		}
	}
}
