package com.lingvo;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class Variables {
	/**
	 * �������� �������� ����
	 */
	public static AnimationDrawable ProgressBar;
	/**
	 * �������� �������� ����
	 */
	public static ImageView imageProgress;
	/**
	 * �������� ������������� ������
	 */
	public static ImageButton image_btn_voice;
	/**
	 * ��� ������ ���������� ������, ����� �����
	 */
	public static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
	/**
	 * � ������ ����� ���������
	 */
	public static String lang_from;
	/**
	 * ������� ���� ���������� ������
	 */
	public static String searchlang;
	/**
	 * ������� �������� ��������� mainWebView
	 */
	public static int scrollY;
	/**
	 * ��������� ��������
	 */
	//public static ImageView hide;
	/**
	 * ����.
	 */
	public static TextView tame;
	/**
	 * ����.
	 */
	public static TextView teng;
	/**
	 * ������ ���������� ����������� ������������
	 */
	public static WebView webViewTran;
	/**
	 * �������� ������������� ����
	 */
	public static MediaPlayer mediaPlayer;
	/**
	 * ������ ������-����������
	 */
	public static ImageButton buttonPlayPauseSong1;	
	/**
	 * ������ ������-����������
	 */
	public static ImageButton buttonPlayPauseSong2;	
	/**
	 * Url ���������� �������� �����
	 */
	public static Uri audiourl;
	/**
	 * ������� ��������� ������������� � ������������� � �������
	 */
	public static RelativeLayout RelativeLayoutWebView;
	/**
	 *  ������ ������� ����
	 *  ������ ��������.
	 */
	public static Button button_translations; 
	/**
	 *  ������ ������� ����
	 *  ������ ��������.
	 */
	public static Button button_examples; 
	/**
	 *  ������ ������� ����
	 *  ������ ��������.
	 */
	public static Button button_revs; 
	/**
	 *  ������ ������� ����
	 *  ������ ����������.
	 */
	public static Button button_interpretation; 
	/**
	 * ������������� ������
	 */
	public static ImageButton imageButtonSwitch;
	/**
	 * ������ � ������� ����� spinnerChoiceLanguageLeft (spinnerChoiceLanguage)
	 */
	public static Spinner sl; 
	/**
	 * ������ � ������� ������ spinnerChoiceLanguageRight (spinnerChoiceLanguage)
	 */
	public static Spinner sr; 
	/**
	 * ������ �������� ���������� ����
	 */
	public static ImageButton cleartextfield;
	/**
	 * ���� ����� ����
	 */
	public static AutoCompleteTextView search_textedit;
	/**
	 * ������ � ���������
	 */
	public static WebView mainwebview;
	/**
	 * ������ ������ ������
	 */
	public static ImageButton image_btn_go;
	/**
	 * A����� ������� ���������� ����� ������ ���� ������ ���������
	 */
	public static String whatButPres= "-1";
	/**
	 * ��� ��������� ���������� �������������
	 */
	public static ScrollView home_scroll;
	
	public static short bs1;
	public static short bs2;
	public static String[] html;
	public static String tmpImg;
	public static String tmpHtml;
	public static String src_song1;
	public static String src_song2;
	public static String url_from;
	public static String go_url;

	/**
	 * ������� �������� ����������
	 */
	public static Context MainContext;
	/**
	 * �������� ��� ������ ��������, � ��� ������� 
	 */
	public static TextView ya_search;
	public static ImageView imageline2;
	public static ImageView imageline3;

}
