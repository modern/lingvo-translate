package com.lingvo;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.RelativeLayout;

/**
 * �������� ������� ������/�������� ��������������. ���������� ��� ������� ������� ������.
 * ������������� ��������� ������������� ��� ������ ��������,
 * � ��� ����� ������� � ����������� �������� 
 * @author VADIM
 *
 */
public class YaProper extends LingvoActivity {
	public Activity activity; 
	private Variables var;
    final View activityRootView; 
    private View layout_search_elements, ya_search_layout, tabBar, include_bottom_buttons, spinners_layout;
    private int  search_elements_h, ya_search_h, spinners_h, tabBar_h;   
	
	YaProper (Activity activity) {
		this.activity = activity;
		
    	layout_search_elements = activity.findViewById(R.id.include_search_elements); 
    	ya_search_layout = activity.findViewById(R.id.ya_linear_layout);
    	tabBar = activity.findViewById(R.id.tabBar);
    	include_bottom_buttons = activity.findViewById(R.id.include_bottom_buttons);
    	spinners_layout = activity.findViewById(R.id.linearLayout1);
		
		activityRootView = this.activity.findViewById(R.id.cont);
		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener()); 
	
		//var.search_textedit.addTextChangedListener(new YaDoInBackground());
	}
	
	class OnGlobalLayoutListener implements android.view.ViewTreeObserver.OnGlobalLayoutListener {

		@Override
		public void onGlobalLayout() {
			// heightDiff - ������ ���������� ���������
			int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight(); // ��� ���������� ���������� ����� �����-�� �������� (38) ��-�� �������� ����
            if (heightDiff > 100) { // ���� ������ 100 ��������, �������� ��� ����������
            	// �������� ������� ��������� ��� ������ ����������
            	search_elements_h = layout_search_elements.getHeight();
            	ya_search_h = ya_search_layout.getHeight();
            	spinners_h = spinners_layout.getHeight();
            	tabBar_h = tabBar.getHeight();
            	var.search_textedit.setDropDownHeight(activityRootView.getHeight() - (search_elements_h + ya_search_h + spinners_h) + tabBar_h); // ������������� ������ ����������� ������

                // �������� ��������� ������ ����� ������ ���������� (layout_search_elements)
                MarginLayoutParams marginParams = new MarginLayoutParams(layout_search_elements.getLayoutParams());
                marginParams.setMargins(0, heightDiff - tabBar_h, 0, 0); 
                RelativeLayout.LayoutParams lParams = new RelativeLayout.LayoutParams(marginParams);
                layout_search_elements.setLayoutParams(lParams);
                
                tabBar.setVisibility(View.GONE);
                include_bottom_buttons.setVisibility(View.GONE);
				ya_search_layout.setVisibility(View.VISIBLE);

            } else if (heightDiff < 100) {
            	include_bottom_buttons.setVisibility(View.VISIBLE);
            	tabBar.setVisibility(View.VISIBLE);
            	ya_search_layout.setVisibility(View.GONE);
                RelativeLayout.LayoutParams lParams = new RelativeLayout.LayoutParams(layout_search_elements.getLayoutParams());
                lParams.addRule(RelativeLayout.ABOVE, R.id.include_bottom_buttons);
                lParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                layout_search_elements.setLayoutParams(lParams);
            }
		}
	}	
}